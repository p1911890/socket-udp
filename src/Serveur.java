import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.sql.SQLOutput;
import java.util.ArrayList;

import static java.lang.Thread.sleep;


public class Serveur{
    public static InetAddress IP_S;
    public static int P_S = 34;
    private static DatagramSocket data;
    private static ArrayList<Integer> PSERV;
    private static ArrayList<InetAddress> ADRSRV;


    public Serveur(){
        PSERV = new ArrayList<Integer>();
        ADRSRV = new ArrayList<InetAddress>();
        setIP_S();
        try{
            data = new DatagramSocket(P_S);
        }catch(Exception e){
            System.out.println("erreur creation ds " + e);
            try {
                sleep(5000);
            }catch (Exception f){}

        }
    }

    public static void setIP_S(){
        try{
            IP_S = InetAddress.getLocalHost();
        }catch(Exception e){
            System.out.println("Le serveur n'a pas été trouvé");
        }
    }

    public static DatagramPacket Recevoir(){
        DatagramPacket dp = new DatagramPacket(new byte[100], 100);
        try{
            data.receive(dp);
        }catch (Exception e){
            System.out.println("CHiant" + e);
        }
        return dp;
    }

    public static void run (){
        DatagramPacket dp;
        String S = "";

        while(true){
            dp = Recevoir();

            try{
                S = new String(dp.getData(),"ascii");
            }catch (Exception e){
                System.out.println("Erreur serveur " +e );
            }
            if (S.contains("quitter serveur")) break;
            if(!S.contains("quitter")) {
                if (!PSERV.contains(dp.getPort())) {
                    PSERV.add(dp.getPort());
                    ADRSRV.add(dp.getAddress());
                }
                new Com(dp.getAddress(), dp.getPort(), S, PSERV, ADRSRV).start();
            }
        }
    }

    public static void main(String args[]){
        Serveur serveur = new Serveur();
        serveur.run();
    }

}

