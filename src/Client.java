
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;
import java.util.Scanner;
import java.io.UnsupportedEncodingException;
import java.lang.String;

import static java.lang.System.exit;

public class Client {
    public static InetAddress IP_S;
    private static DatagramSocket data;
    public static int P_S = 34;

    public Client(){
        setIP_C();
        try{
            data = new DatagramSocket();
        }catch(Exception e){
        }
        Init();
    }

    public static void setIP_C(){
        try{
            IP_S = InetAddress.getLocalHost();
        }catch(Exception e){
            System.out.println("Le serveur n'a pas été trouvé");
        }
    }

    public static void Envoi(){
        Scanner str = new Scanner(System.in);
        System.out.println("Entrez une phrase");
        String phrase = str.nextLine();
        try{
            byte[] byt = phrase.getBytes("ascii");
            try{
                try {
                    DatagramPacket dp = new DatagramPacket(byt,byt.length,IP_S ,P_S );
                    data.send(dp);
                }catch(Exception e){}
            }catch(Exception e){
                System.out.println("Le serveur n'a pas été trouvé");
            }
        }catch(Exception e){}
        if(phrase.contains("quitter"))exit(0);
    }

    public static void Init(){
        String phrase = "ejqlzjdqkzdqljdqlkdjq0/";
        try{
            byte[] byt = phrase.getBytes("ascii");
            try{
                try {
                    DatagramPacket dp = new DatagramPacket(byt,byt.length,IP_S ,P_S );
                    data.send(dp);
                }catch(Exception e){}
            }catch(Exception e){
                System.out.println("Le serveur n'a pas été trouvé");
            }
        }catch(Exception e){}
        System.out.println("Connexion fonctionnelle");
    }

    public static DatagramPacket Recevoir(){
        DatagramPacket dp = new DatagramPacket(new byte[100], 100);
        try{
            data.receive(dp);
            String S = new String(dp.getData(),"ascii").split("\0")[0];
            System.out.println(S);
        }catch (Exception e){
            System.out.println("Erreur " + e);
        }
        return dp;
    }

    public static boolean Choix(){
        Scanner str = new Scanner(System.in);
        System.out.println("Faites votre choix, souhaitez vous ecouter (tapez 1), ou ecrire (tapez n'importe quoi d'autre)");
        String phrase = str.nextLine();
        if(phrase.equals("1")){
            System.out.println("Mode Ecoute active");
            return false;
        }else{return true;}
    }

    public static void main(String args[]){
        Client c = new Client();
        if(c.Choix()) {
            new Thread() {
                public void run() {
                    DatagramPacket dp;
                    String S = "";

                    while (true) {
                        Envoi();
                    }
                }
            }.start();
        }else{
            new Thread() {
                public void run() {
                    DatagramPacket dp;
                    String S = "";

                    while (true) {
                        for (int i = 0; i < 2; i++) {
                            dp = Recevoir();

                            try {
                                S = new String(dp.getData(), "ascii");
                            } catch (Exception e) {
                                System.out.println("Erreur serveur " + e);
                            }
                        }
                        if (S.contains("quitter ecoute")) break;
                    }
                }
            }.start();
        }
    }

}
